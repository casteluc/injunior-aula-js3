const transformLists = (...lists) => {
    let newList = []
    
    let biggerList = []
    
    lists.forEach( list => {
        if (list.length > biggerList.length) {
            biggerList = list
        }
    })

    biggerList.forEach ( num => {
        let inList = true
        for (list of lists) {
            if (!list.includes(num) || newList.includes(num)) {
                inList = false 
            }
        }
    
        if (inList) {
            newList.push(num)
        }
    })
    
    return newList
}

console.log(transformLists([1, 2, 3], [3, 3, 7], [9, 111, 3]))


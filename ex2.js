const multiplyList = (n, list) => {
    return list.map( value => value * n)
}

console.log(multiplyList(2, [1,3,6,10]))
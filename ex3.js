const mapArray = (string, stringArray) => {
    let finalArray = stringArray.filter( s => {
        let inArray = true

        for (char of s) {
            if ( !string.includes(char)) {
                inArray = false
            }
        }

        if (inArray) {
            return s
        }
    })

    return finalArray
}

console.log(mapArray("ab", ["abc", "ba", "ab", "bb", "kb"]))
const sumList = (...lists) => {
    let total = 0
    lists.forEach( list => {
        list.forEach( num => {
            total += num
        })
    } )

    return total
}

console.log(sumList([1,2,3], [1,2,2], [1,1]))